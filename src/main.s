; Filename:     main.s 
; Author:        

    export __main
    export ALERT_LED
    export ALERT_ROTATE
	export ACTIVE_COL
	export LED_STRING
	export LED_DATA_OUT
	export LED_GREETING
    import initializeBoard
	import ledMatrixWriteData
	import rotate_array
	import initialize_led_array

    
;**********************************************
; SRAM
;**********************************************
    AREA    SRAM, READWRITE
ALERT_LED       DCB     0xFF
ALERT_ROTATE    DCB     0xFF
ACTIVE_COL		DCB 	0xFF
LED_DATA_OUT	DCB		0xFF
LED_STRING 		SPACE	1*50
	

    align
        
;**********************************************
; FLASH Read Only Constants
;**********************************************
    AREA    |.text|, CODE, READONLY
I2C1_BASE		DCD		0x40021000
LED_GREETING	DCB	0x0E
				DCB 0x0C
				DCB 0x0E
				DCB 0x03
				DCB 0x05
				DCB 0x03
				DCB 0xFF
				DCB 0xFF
				DCB 0xFF
				DCB 0xFF

;**********************************************
; Code (FLASH) Segment
; main assembly program
;**********************************************
__main PROC
    ; Initialize SRAM variables
	LDR		R4, =(ALERT_LED)
	LDR		R5, =(ALERT_ROTATE)
	LDR		R6, =(ACTIVE_COL)
	LDR		R7, =(LED_DATA_OUT)
	LDR		R10, =(LED_STRING)
	MOV		R0,	#0
	STRB	R0,[R4]
	STRB	R0,[R5]
	STRB	R0,[R6]
	STRB	R0,[R7]
	
	; Initialize the board
    LDR     R0, =(initializeBoard)
    BLX     R0
	
	LDR		R0, =(initialize_led_array)
	BLX		R0

ForEver 

	; (1)
	LDRB	R0, [R4]	;ALERT_LED
	TST 	R0,R0
	BLNE	Inc_Active_Col	; If ALERT_LED != 0
	MOVNE	R0, #0
	STRBNE	R0, [R4]	; Reset ALERT_LED = 0
	LDRB	R0, [R5]	; ALERT_ROTATE
	TST	R0,R0
	MOVNE	R0,#0
	STRBNE	R0,	[R5]
	LDR		R0,=(LED_STRING)
	MOV		R1,#50
	BLNE	rotate_array
	
    B       ForEver
	
Inc_Active_Col
	LDRB	R0, [R6]
	CMP		R0, #4
	ADDNE	R0, #1
	MOVEQ	R0, #0
	STRB	R0, [R6]	
	; (2)
	; If ACTIVE_COL is modified ledMatrixWriteData
	MOV		R9, LR	; Save LR
	LDR 	R8,=(ledMatrixWriteData)
	MOV		R1, R0 	; Set second argument as ACTIVE_COL
	LDR		R0, I2C1_BASE
	LDRB	R2,	[R10,R1]
	BLX		R8
	MOV		LR, R9
	BX		LR
    
    ENDP
    ALIGN
        
    END
        