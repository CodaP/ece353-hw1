; Filename:     led_display.s 
; Author:       Your Name HERE 

	export rotate_array

;**********************************************
; SRAM
;**********************************************
    AREA    SRAM, READWRITE
    align
        
;**********************************************
; FLASH Read Only Constants
;**********************************************
    AREA    |.text|, CODE, READONLY

;**********************************************
; Routine to rotate the array used to store
; the LED matrix message
;**********************************************
	; R0 = [array]
	; R1 = array size
rotate_array PROC
	TST R1,R1
	BEQ END_LOOP
	LDRB R2, [R0,#0]
	ADD R1,R0
	SUB R1, #1
BEGIN
	CMP R1,R0
	BEQ	END_LOOP
	LDRB R3, [R0, #1]
	STRB R3, [R0], #1
	B BEGIN
END_LOOP
	STRB R2, [R1]
	BX	LR
    ENDP
        
    END